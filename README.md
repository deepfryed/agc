## Automatic audio leveling / compressor

This is a audio compressor based on [w2aew's circuit tutorial](https://www.youtube.com/watch?v=1h0FZJYXQ_w)

It can automatically level the audio and smooth out the spikes and crackling noise encountered in various audio receivers. Quite helpful for radio scanners, ultrasound audio doppler measurements etc.

# License

OSHW - free to modify and use, please include a reference to the original YouTube tutorial.
