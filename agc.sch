EESchema Schematic File Version 4
LIBS:agc-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Audio Auto Gain Control"
Date "2019-02-01"
Rev "v0.1"
Comp "deepfryed"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L agc-rescue:Battery_Cell BT1
U 1 1 5A3BA356
P 3580 1600
F 0 "BT1" H 3680 1700 39  0000 L CNN
F 1 "LIPO" H 3680 1600 39  0000 L CNN
F 2 "modules:S2B-PH-SM4-TB" V 3580 1660 50  0001 C CNN
F 3 "" V 3580 1660 50  0001 C CNN
	1    3580 1600
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:C_Small C6
U 1 1 5A5BD300
P 3270 1530
F 0 "C6" H 3280 1600 39  0000 L CNN
F 1 "4.7uF" H 3280 1450 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 3270 1530 50  0001 C CNN
F 3 "" H 3270 1530 50  0001 C CNN
	1    3270 1530
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5A5BD452
P 3580 2360
F 0 "#PWR01" H 3580 2110 50  0001 C CNN
F 1 "GND" H 3580 2210 39  0000 C CNN
F 2 "" H 3580 2360 50  0001 C CNN
F 3 "" H 3580 2360 50  0001 C CNN
	1    3580 2360
	1    0    0    -1  
$EndComp
$Comp
L mcp73831:MCP73831 U2
U 1 1 5A5BD506
P 2330 1520
F 0 "U2" H 2130 1060 60  0000 C CNN
F 1 "MCP73831" H 2440 1060 39  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 2330 1170 60  0001 C CNN
F 3 "" H 2330 1170 60  0000 C CNN
	1    2330 1520
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R4
U 1 1 5A5BD96F
P 3010 1690
F 0 "R4" H 3040 1710 39  0000 L CNN
F 1 "2K" H 3040 1650 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 3010 1690 50  0001 C CNN
F 3 "" H 3010 1690 50  0001 C CNN
	1    3010 1690
	1    0    0    -1  
$EndComp
$Comp
L USB_B_MICRO:MICRO-B_USB U1
U 1 1 5A5BDB08
P 1070 1520
F 0 "U1" H 1070 1220 39  0000 C CNN
F 1 "MICRO-B_USB" H 1070 1820 50  0001 C CNN
F 2 "modules:USB_MICRO_B_FCI-10118194-0001LF" H 1070 1520 60  0001 C CNN
F 3 "" H 1070 1520 60  0000 C CNN
	1    1070 1520
	1    0    0    -1  
$EndComp
NoConn ~ 1270 1420
NoConn ~ 1270 1520
NoConn ~ 1270 1620
$Comp
L power:GND #PWR02
U 1 1 5A5BDC73
P 1500 2360
F 0 "#PWR02" H 1500 2110 50  0001 C CNN
F 1 "GND" H 1500 2210 39  0000 C CNN
F 2 "" H 1500 2360 50  0001 C CNN
F 3 "" H 1500 2360 50  0001 C CNN
	1    1500 2360
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R2
U 1 1 5A5BDD3C
P 1670 1460
F 0 "R2" H 1700 1480 39  0000 L CNN
F 1 "200R" V 1830 1360 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 1670 1460 50  0001 C CNN
F 3 "" H 1670 1460 50  0001 C CNN
	1    1670 1460
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:LED_Small_ALT D1
U 1 1 5A5BDDA6
P 1670 1690
F 0 "D1" H 1640 1600 39  0000 L CNN
F 1 "LED_Small_ALT" H 1495 1590 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" V 1670 1690 50  0001 C CNN
F 3 "" V 1670 1690 50  0001 C CNN
	1    1670 1690
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:C_Small C2
U 1 1 5A5BEA84
P 1500 1510
F 0 "C2" H 1510 1580 39  0000 L CNN
F 1 "4.7uF" V 1400 1410 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 1500 1510 50  0001 C CNN
F 3 "" H 1500 1510 50  0001 C CNN
	1    1500 1510
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5A5BF7F8
P 1230 5230
F 0 "#PWR03" H 1230 4980 50  0001 C CNN
F 1 "GND" H 1230 5080 39  0000 C CNN
F 2 "" H 1230 5230 50  0001 C CNN
F 3 "" H 1230 5230 50  0001 C CNN
	1    1230 5230
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5A5BFA2A
P 7765 5270
F 0 "#PWR04" H 7765 5020 50  0001 C CNN
F 1 "GND" H 7765 5120 39  0000 C CNN
F 2 "" H 7765 5270 50  0001 C CNN
F 3 "" H 7765 5270 50  0001 C CNN
	1    7765 5270
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR05
U 1 1 5A5BFC57
P 3580 1220
F 0 "#PWR05" H 3580 1070 50  0001 C CNN
F 1 "+BATT" H 3580 1360 39  0000 C CNN
F 2 "" H 3580 1220 50  0001 C CNN
F 3 "" H 3580 1220 50  0001 C CNN
	1    3580 1220
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR06
U 1 1 5A5BFCFD
P 4275 1220
F 0 "#PWR06" H 4275 1070 50  0001 C CNN
F 1 "+BATT" H 4275 1360 39  0000 C CNN
F 2 "" H 4275 1220 50  0001 C CNN
F 3 "" H 4275 1220 50  0001 C CNN
	1    4275 1220
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:C_Small C3
U 1 1 5A5C13B1
P 2200 3340
F 0 "C3" H 2210 3410 39  0000 L CNN
F 1 "0.22uF" V 2080 3210 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2200 3340 50  0001 C CNN
F 3 "" H 2200 3340 50  0001 C CNN
	1    2200 3340
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:R_Small R3
U 1 1 5A5C15A5
P 2650 3340
F 0 "R3" H 2680 3360 39  0000 L CNN
F 1 "10K" V 2540 3240 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 2650 3340 50  0001 C CNN
F 3 "" H 2650 3340 50  0001 C CNN
	1    2650 3340
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:C_Small C8
U 1 1 5A5C1A18
P 4900 3340
F 0 "C8" H 4910 3410 39  0000 L CNN
F 1 "0.22uF" V 4780 3210 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 4900 3340 50  0001 C CNN
F 3 "" H 4900 3340 50  0001 C CNN
	1    4900 3340
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:R_Small R9
U 1 1 5A5C1C71
P 5230 3700
F 0 "R9" H 5260 3720 39  0000 L CNN
F 1 "100K" V 5120 3600 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 5230 3700 50  0001 C CNN
F 3 "" H 5230 3700 50  0001 C CNN
	1    5230 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5A5C1E90
P 5230 5230
F 0 "#PWR07" H 5230 4980 50  0001 C CNN
F 1 "GND" H 5230 5080 39  0000 C CNN
F 2 "" H 5230 5230 50  0001 C CNN
F 3 "" H 5230 5230 50  0001 C CNN
	1    5230 5230
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:D_Small_ALT D2
U 1 1 5A5C27BD
P 2240 4780
F 0 "D2" H 2190 4860 39  0000 L CNN
F 1 "Germanium" H 2090 4700 50  0001 L CNN
F 2 "Diode_THT:D_DO-15_P10.16mm_Horizontal" V 2240 4780 50  0001 C CNN
F 3 "" V 2240 4780 50  0001 C CNN
	1    2240 4780
	-1   0    0    1   
$EndComp
$Comp
L agc-rescue:D_Small_ALT D3
U 1 1 5A5C2994
P 2610 4780
F 0 "D3" H 2560 4860 39  0000 L CNN
F 1 "Germanium" H 2460 4700 50  0001 L CNN
F 2 "Diode_THT:D_DO-15_P10.16mm_Horizontal" V 2610 4780 50  0001 C CNN
F 3 "" V 2610 4780 50  0001 C CNN
	1    2610 4780
	-1   0    0    1   
$EndComp
$Comp
L agc-rescue:Q_NPN_BEC Q1
U 1 1 5A5C2B99
P 1700 4780
F 0 "Q1" H 1570 4630 39  0000 L CNN
F 1 "MMBT2222A" H 1230 4550 39  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 1900 4880 50  0001 C CNN
F 3 "" H 1700 4780 50  0001 C CNN
	1    1700 4780
	-1   0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R1
U 1 1 5A5C327C
P 1600 4330
F 0 "R1" H 1660 4350 39  0000 L CNN
F 1 "10K" H 1650 4280 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 1600 4330 50  0001 C CNN
F 3 "" H 1600 4330 50  0001 C CNN
	1    1600 4330
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5A5C3703
P 1600 5230
F 0 "#PWR08" H 1600 4980 50  0001 C CNN
F 1 "GND" H 1600 5080 39  0000 C CNN
F 2 "" H 1600 5230 50  0001 C CNN
F 3 "" H 1600 5230 50  0001 C CNN
	1    1600 5230
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:C_Small C4
U 1 1 5A5C3E28
P 2200 3770
F 0 "C4" H 2210 3840 39  0000 L CNN
F 1 "0.47uF" V 2080 3640 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2200 3770 50  0001 C CNN
F 3 "" H 2200 3770 50  0001 C CNN
	1    2200 3770
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:C_Small C5
U 1 1 5A5C560B
P 2880 4990
F 0 "C5" H 2940 5100 39  0000 L CNN
F 1 "2.2uF" V 2990 4830 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 2880 4990 50  0001 C CNN
F 3 "" H 2880 4990 50  0001 C CNN
	1    2880 4990
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5A5C5750
P 2880 5230
F 0 "#PWR09" H 2880 4980 50  0001 C CNN
F 1 "GND" H 2880 5080 39  0000 C CNN
F 2 "" H 2880 5230 50  0001 C CNN
F 3 "" H 2880 5230 50  0001 C CNN
	1    2880 5230
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R5
U 1 1 5A5C59C3
P 3180 4990
F 0 "R5" H 3040 4920 39  0000 L CNN
F 1 "240K" V 3070 4970 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 3180 4990 50  0001 C CNN
F 3 "" H 3180 4990 50  0001 C CNN
	1    3180 4990
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5A5C5E29
P 3180 5230
F 0 "#PWR010" H 3180 4980 50  0001 C CNN
F 1 "GND" H 3180 5080 39  0000 C CNN
F 2 "" H 3180 5230 50  0001 C CNN
F 3 "" H 3180 5230 50  0001 C CNN
	1    3180 5230
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:Q_NPN_BEC Q2
U 1 1 5A5C6285
P 3830 4780
F 0 "Q2" H 4030 4850 39  0000 L CNN
F 1 "MMBT2222A" H 4030 4780 39  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4030 4880 50  0001 C CNN
F 3 "" H 3830 4780 50  0001 C CNN
	1    3830 4780
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R6
U 1 1 5A5C63BA
P 3410 4780
F 0 "R6" V 3300 4630 39  0000 L CNN
F 1 "100R" V 3300 4740 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 3410 4780 50  0001 C CNN
F 3 "" H 3410 4780 50  0001 C CNN
	1    3410 4780
	0    1    1    0   
$EndComp
$Comp
L agc-rescue:D_Small_ALT D4
U 1 1 5A5C6E93
P 4480 5040
F 0 "D4" H 4430 5120 39  0000 L CNN
F 1 "Germanium" H 4330 4960 50  0001 L CNN
F 2 "Diode_THT:D_DO-15_P10.16mm_Horizontal" V 4480 5040 50  0001 C CNN
F 3 "" V 4480 5040 50  0001 C CNN
	1    4480 5040
	-1   0    0    1   
$EndComp
$Comp
L agc-rescue:D_Small_ALT D6
U 1 1 5A5C6E99
P 4850 5040
F 0 "D6" H 4800 5120 39  0000 L CNN
F 1 "Germanium" H 4700 4960 50  0001 L CNN
F 2 "Diode_THT:D_DO-15_P10.16mm_Horizontal" V 4850 5040 50  0001 C CNN
F 3 "" V 4850 5040 50  0001 C CNN
	1    4850 5040
	-1   0    0    1   
$EndComp
$Comp
L agc-rescue:R_Small R7
U 1 1 5A5C6F45
P 4190 5040
F 0 "R7" V 4080 4960 39  0000 L CNN
F 1 "6.8K" V 4080 5070 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 4190 5040 50  0001 C CNN
F 3 "" H 4190 5040 50  0001 C CNN
	1    4190 5040
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5A5C7B3D
P 5040 5230
F 0 "#PWR011" H 5040 4980 50  0001 C CNN
F 1 "GND" H 5040 5080 39  0000 C CNN
F 2 "" H 5040 5230 50  0001 C CNN
F 3 "" H 5040 5230 50  0001 C CNN
	1    5040 5230
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR012
U 1 1 5A5C8781
P 4820 1230
F 0 "#PWR012" H 4820 1080 50  0001 C CNN
F 1 "VCC" H 4820 1380 39  0000 C CNN
F 2 "" H 4820 1230 50  0001 C CNN
F 3 "" H 4820 1230 50  0001 C CNN
	1    4820 1230
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R8
U 1 1 5A5C9015
P 4820 1880
F 0 "R8" H 4850 1900 39  0000 L CNN
F 1 "200R" V 4720 1800 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 4820 1880 50  0001 C CNN
F 3 "" H 4820 1880 50  0001 C CNN
	1    4820 1880
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:LED_Small_ALT D5
U 1 1 5A5C90D2
P 4820 2170
F 0 "D5" H 4790 2080 39  0000 L CNN
F 1 "LED_Small_ALT" H 4645 2070 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" V 4820 2170 50  0001 C CNN
F 3 "" V 4820 2170 50  0001 C CNN
	1    4820 2170
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5A5C9368
P 4820 2360
F 0 "#PWR013" H 4820 2110 50  0001 C CNN
F 1 "GND" H 4820 2210 39  0000 C CNN
F 2 "" H 4820 2360 50  0001 C CNN
F 3 "" H 4820 2360 50  0001 C CNN
	1    4820 2360
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:R_Small R10
U 1 1 5A5CA6D9
P 6820 1210
F 0 "R10" V 6900 1140 39  0000 L CNN
F 1 "1K" V 6750 1175 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 6820 1210 50  0001 C CNN
F 3 "" H 6820 1210 50  0001 C CNN
	1    6820 1210
	0    1    1    0   
$EndComp
$Comp
L agc-rescue:LED_Small_ALT D9
U 1 1 5A5CA6DF
P 7210 1465
F 0 "D9" H 7180 1375 39  0000 L CNN
F 1 "LED_Small_ALT" H 7035 1365 50  0001 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" V 7210 1465 50  0001 C CNN
F 3 "" V 7210 1465 50  0001 C CNN
	1    7210 1465
	0    -1   -1   0   
$EndComp
Text Notes 1200 5770 0    39   ~ 0
D2, D3, D4, D6, D7, D8 should be germanium or schottky.
Wire Wire Line
	2880 1820 3010 1820
Wire Wire Line
	2880 1570 3010 1570
Wire Wire Line
	3010 1570 3010 1590
Wire Wire Line
	3010 1790 3010 1820
Connection ~ 3010 1820
Wire Wire Line
	1270 1320 1500 1320
Wire Wire Line
	1500 1610 1500 1720
Wire Wire Line
	1500 1720 1270 1720
Wire Wire Line
	1670 1360 1670 1320
Connection ~ 1670 1320
Wire Wire Line
	1670 1560 1670 1590
Wire Wire Line
	1670 1790 1670 1820
Wire Wire Line
	1670 1820 1780 1820
Connection ~ 1500 1720
Wire Wire Line
	1500 1410 1500 1320
Connection ~ 1500 1320
Wire Wire Line
	2880 1320 3270 1320
Wire Wire Line
	3580 1220 3580 1320
Wire Wire Line
	3580 1700 3580 1820
Connection ~ 3580 1820
Wire Wire Line
	3270 1430 3270 1320
Connection ~ 3270 1320
Wire Wire Line
	3270 1630 3270 1820
Connection ~ 3270 1820
Connection ~ 3580 1320
Wire Wire Line
	2300 3340 2550 3340
Wire Wire Line
	1150 3340 1910 3340
Wire Wire Line
	5000 3340 5230 3340
Wire Wire Line
	1230 3440 1230 5230
Wire Wire Line
	2750 3340 4670 3340
Wire Wire Line
	1900 4780 2025 4780
Wire Wire Line
	1600 4430 1600 4505
Wire Wire Line
	1910 3340 1910 3770
Wire Wire Line
	1910 3770 2100 3770
Connection ~ 1910 3340
Wire Wire Line
	2300 3770 2430 3770
Wire Wire Line
	2340 4780 2430 4780
Wire Wire Line
	2430 3770 2430 4780
Connection ~ 2430 4780
Wire Wire Line
	2880 5090 2880 5230
Wire Wire Line
	2710 4780 2880 4780
Wire Wire Line
	2880 4780 2880 4890
Wire Wire Line
	3180 5090 3180 5230
Wire Wire Line
	3180 4780 3180 4890
Connection ~ 2880 4780
Connection ~ 3180 4780
Wire Wire Line
	3510 4780 3630 4780
Wire Wire Line
	4580 5040 4670 5040
Wire Wire Line
	3930 4980 3930 5040
Wire Wire Line
	3930 5040 4090 5040
Wire Wire Line
	4290 5040 4380 5040
Wire Wire Line
	4670 5040 4670 3340
Connection ~ 4670 3340
Connection ~ 4670 5040
Wire Wire Line
	4950 5040 5040 5040
Wire Wire Line
	5040 5040 5040 5230
Wire Wire Line
	4275 1525 4275 1220
Wire Wire Line
	4820 1230 4820 1425
Wire Wire Line
	4820 2070 4820 1980
Wire Wire Line
	4820 2360 4820 2270
Wire Wire Line
	5230 3600 5230 3340
Connection ~ 5230 3340
Wire Wire Line
	1600 4505 2025 4505
Wire Wire Line
	2025 4505 2025 4780
Connection ~ 2025 4780
Connection ~ 1600 4505
$Comp
L power:VCC #PWR016
U 1 1 5B64EDD9
P 1600 4120
F 0 "#PWR016" H 1600 3970 50  0001 C CNN
F 1 "VCC" H 1600 4270 39  0000 C CNN
F 2 "" H 1600 4120 50  0001 C CNN
F 3 "" H 1600 4120 50  0001 C CNN
	1    1600 4120
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR017
U 1 1 5B6507EC
P 3930 4130
F 0 "#PWR017" H 3930 3980 50  0001 C CNN
F 1 "VCC" H 3930 4280 39  0000 C CNN
F 2 "" H 3930 4130 50  0001 C CNN
F 3 "" H 3930 4130 50  0001 C CNN
	1    3930 4130
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5B65115C
P 6430 2240
F 0 "#PWR018" H 6430 1990 50  0001 C CNN
F 1 "GND" H 6430 2090 39  0000 C CNN
F 2 "" H 6430 2240 50  0001 C CNN
F 3 "" H 6430 2240 50  0001 C CNN
	1    6430 2240
	1    0    0    -1  
$EndComp
Wire Wire Line
	6430 2240 6430 2090
$Comp
L power:VCC #PWR019
U 1 1 5B651337
P 6430 1145
F 0 "#PWR019" H 6430 995 50  0001 C CNN
F 1 "VCC" H 6430 1295 39  0000 C CNN
F 2 "" H 6430 1145 50  0001 C CNN
F 3 "" H 6430 1145 50  0001 C CNN
	1    6430 1145
	1    0    0    -1  
$EndComp
Wire Wire Line
	6430 1145 6430 1210
Wire Wire Line
	6720 1210 6430 1210
Connection ~ 6430 1210
Wire Wire Line
	6920 1210 7210 1210
$Comp
L agc-rescue:PAM8302A U4
U 1 1 5B65458E
P 7765 4635
F 0 "U4" H 7665 5135 50  0000 R TNN
F 1 "PAM8302A" H 7665 5035 50  0000 R TNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7665 4535 50  0001 C CNN
F 3 "" H 7665 4535 50  0001 C CNN
	1    7765 4635
	1    0    0    -1  
$EndComp
Wire Wire Line
	7765 5270 7765 5035
$Comp
L agc-rescue:POT RV1
U 1 1 5B655555
P 6445 3915
F 0 "RV1" V 6270 3915 50  0000 C CNN
F 1 "10K" V 6345 3915 39  0000 C CNN
F 2 "Potentiometers:Potentiometer_Alps_RK163_Single_Vertical" H 6445 3915 50  0001 C CNN
F 3 "" H 6445 3915 50  0001 C CNN
	1    6445 3915
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5B6556AF
P 6445 5275
F 0 "#PWR020" H 6445 5025 50  0001 C CNN
F 1 "GND" H 6445 5125 39  0000 C CNN
F 2 "" H 6445 5275 50  0001 C CNN
F 3 "" H 6445 5275 50  0001 C CNN
	1    6445 5275
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR021
U 1 1 5B65704E
P 7765 3905
F 0 "#PWR021" H 7765 3755 50  0001 C CNN
F 1 "VCC" H 7765 4055 39  0000 C CNN
F 2 "" H 7765 3905 50  0001 C CNN
F 3 "" H 7765 3905 50  0001 C CNN
	1    7765 3905
	1    0    0    -1  
$EndComp
Wire Wire Line
	7765 3905 7765 4040
Wire Wire Line
	6445 4065 6445 4635
$Comp
L agc-rescue:C_Small C9
U 1 1 5B657E8A
P 6830 4435
F 0 "C9" V 6860 4475 39  0000 L CNN
F 1 "1uF" V 6870 4285 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6830 4435 50  0001 C CNN
F 3 "" H 6830 4435 50  0001 C CNN
	1    6830 4435
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:C_Small C10
U 1 1 5B6581F3
P 6830 4635
F 0 "C10" V 6865 4680 39  0000 L CNN
F 1 "1uF" V 6865 4485 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 6830 4635 50  0001 C CNN
F 3 "" H 6830 4635 50  0001 C CNN
	1    6830 4635
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:R_Small R11
U 1 1 5B6587BB
P 7145 4435
F 0 "R11" V 7205 4385 39  0000 L CNN
F 1 "100" V 7080 4385 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 7145 4435 50  0001 C CNN
F 3 "" H 7145 4435 50  0001 C CNN
	1    7145 4435
	0    1    1    0   
$EndComp
$Comp
L agc-rescue:R_Small R12
U 1 1 5B658FF9
P 7145 4635
F 0 "R12" V 7205 4575 39  0000 L CNN
F 1 "100" V 7085 4585 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 7145 4635 50  0001 C CNN
F 3 "" H 7145 4635 50  0001 C CNN
	1    7145 4635
	0    1    1    0   
$EndComp
Wire Wire Line
	7245 4635 7365 4635
Wire Wire Line
	7045 4635 6930 4635
Wire Wire Line
	6930 4435 7045 4435
Wire Wire Line
	7245 4435 7365 4435
Wire Wire Line
	6595 3915 6680 3915
Wire Wire Line
	6680 3915 6680 4435
Wire Wire Line
	6680 4435 6730 4435
Wire Wire Line
	6445 4635 6730 4635
Connection ~ 6445 4635
$Comp
L power:VCC #PWR022
U 1 1 5B65A7A3
P 6900 4835
F 0 "#PWR022" H 6900 4685 50  0001 C CNN
F 1 "VCC" H 6900 4985 39  0000 C CNN
F 2 "" H 6900 4835 50  0001 C CNN
F 3 "" H 6900 4835 50  0001 C CNN
	1    6900 4835
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:R_Small R13
U 1 1 5B65A9A6
P 7145 4835
F 0 "R13" V 7205 4780 39  0000 L CNN
F 1 "100K" V 7085 4760 39  0000 L CNN
F 2 "Resistors_SMD:R_0805" H 7145 4835 50  0001 C CNN
F 3 "" H 7145 4835 50  0001 C CNN
	1    7145 4835
	0    1    1    0   
$EndComp
$Comp
L agc-rescue:C_Small C11
U 1 1 5B65B091
P 8015 4040
F 0 "C11" V 7905 4005 39  0000 L CNN
F 1 "10uF" V 8120 3975 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8015 4040 50  0001 C CNN
F 3 "" H 8015 4040 50  0001 C CNN
	1    8015 4040
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7765 4040 7915 4040
Connection ~ 7765 4040
$Comp
L power:GND #PWR023
U 1 1 5B65BA3E
P 8270 4040
F 0 "#PWR023" H 8270 3790 50  0001 C CNN
F 1 "GND" H 8270 3890 39  0000 C CNN
F 2 "" H 8270 4040 50  0001 C CNN
F 3 "" H 8270 4040 50  0001 C CNN
	1    8270 4040
	1    0    0    -1  
$EndComp
Wire Wire Line
	8115 4040 8270 4040
Wire Wire Line
	7245 4835 7365 4835
Wire Wire Line
	6900 4835 7045 4835
$Comp
L agc-rescue:Ferrite_Bead_Small L2
U 1 1 5B65E3C5
P 8380 4435
F 0 "L2" V 8310 4335 39  0000 L CNN
F 1 "Ferrite_Bead_Small" H 8455 4385 50  0001 L CNN
F 2 "Inductors_SMD:L_0805" V 8310 4435 50  0001 C CNN
F 3 "" H 8380 4435 50  0001 C CNN
	1    8380 4435
	0    1    1    0   
$EndComp
Wire Wire Line
	8165 4435 8280 4435
Wire Wire Line
	8480 4435 8520 4435
$Comp
L agc-rescue:Ferrite_Bead_Small L1
U 1 1 5B65EAA4
P 8370 4635
F 0 "L1" V 8310 4545 39  0000 L CNN
F 1 "Ferrite_Bead_Small" H 8445 4585 50  0001 L CNN
F 2 "Inductors_SMD:L_0805" V 8300 4635 50  0001 C CNN
F 3 "" H 8370 4635 50  0001 C CNN
	1    8370 4635
	0    1    1    0   
$EndComp
Wire Wire Line
	8270 4635 8165 4635
$Comp
L agc-rescue:C_Small C12
U 1 1 5B65F301
P 8520 4890
F 0 "C12" V 8410 4855 39  0000 L CNN
F 1 "220pF" H 8330 4810 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8520 4890 50  0001 C CNN
F 3 "" H 8520 4890 50  0001 C CNN
	1    8520 4890
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:C_Small C13
U 1 1 5B65F856
P 8780 4890
F 0 "C13" V 8670 4855 39  0000 L CNN
F 1 "220pF" H 8590 4810 39  0000 L CNN
F 2 "Capacitors_SMD:C_0805" H 8780 4890 50  0001 C CNN
F 3 "" H 8780 4890 50  0001 C CNN
	1    8780 4890
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5B65FA40
P 8660 5270
F 0 "#PWR024" H 8660 5020 50  0001 C CNN
F 1 "GND" H 8660 5120 39  0000 C CNN
F 2 "" H 8660 5270 50  0001 C CNN
F 3 "" H 8660 5270 50  0001 C CNN
	1    8660 5270
	1    0    0    -1  
$EndComp
Wire Wire Line
	8520 4990 8520 5140
Wire Wire Line
	8520 5140 8660 5140
Wire Wire Line
	8780 5140 8780 4990
Wire Wire Line
	8660 5270 8660 5140
Connection ~ 8660 5140
Wire Wire Line
	8520 4435 8520 4790
Wire Wire Line
	8780 4635 8780 4790
Wire Wire Line
	6445 3340 6445 3765
$Comp
L agc-rescue:SW_SPDT_MSM SW1
U 1 1 5B6824CA
P 4475 1525
F 0 "SW1" H 4475 1725 39  0000 C CNN
F 1 "SW_SPDT_MSM" H 4475 1325 50  0001 C CNN
F 2 "modules:SPDT-3-4.5mm" H 4475 1525 50  0001 C CNN
F 3 "" H 4475 1525 50  0001 C CNN
	1    4475 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	4675 1425 4820 1425
Connection ~ 4820 1425
Wire Wire Line
	4675 1625 4820 1625
Connection ~ 4820 1625
$Comp
L agc-rescue:Conn_01x01 J3
U 1 1 5B6E8ECC
P 1300 6500
F 0 "J3" H 1300 6600 39  0000 C CNN
F 1 "Conn_01x01" H 1300 6400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 1300 6500 50  0001 C CNN
F 3 "" H 1300 6500 50  0001 C CNN
	1    1300 6500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5B6E96C2
P 1300 6900
F 0 "#PWR025" H 1300 6650 50  0001 C CNN
F 1 "GND" H 1300 6750 39  0000 C CNN
F 2 "" H 1300 6900 50  0001 C CNN
F 3 "" H 1300 6900 50  0001 C CNN
	1    1300 6900
	1    0    0    -1  
$EndComp
$Comp
L agc-rescue:Conn_01x01 J4
U 1 1 5B6E99B0
P 1500 6500
F 0 "J4" H 1500 6600 39  0000 C CNN
F 1 "Conn_01x01" H 1500 6400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 1500 6500 50  0001 C CNN
F 3 "" H 1500 6500 50  0001 C CNN
	1    1500 6500
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:Conn_01x01 J5
U 1 1 5B6E9A56
P 1700 6500
F 0 "J5" H 1700 6600 39  0000 C CNN
F 1 "Conn_01x01" H 1700 6400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 1700 6500 50  0001 C CNN
F 3 "" H 1700 6500 50  0001 C CNN
	1    1700 6500
	0    -1   -1   0   
$EndComp
$Comp
L agc-rescue:Conn_01x01 J6
U 1 1 5B6E9AFF
P 1900 6500
F 0 "J6" H 1900 6600 39  0000 C CNN
F 1 "Conn_01x01" H 1900 6400 50  0001 C CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3_DIN965_Pad" H 1900 6500 50  0001 C CNN
F 3 "" H 1900 6500 50  0001 C CNN
	1    1900 6500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5B6E9BAB
P 1500 6900
F 0 "#PWR026" H 1500 6650 50  0001 C CNN
F 1 "GND" H 1500 6750 39  0000 C CNN
F 2 "" H 1500 6900 50  0001 C CNN
F 3 "" H 1500 6900 50  0001 C CNN
	1    1500 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5B6E9C52
P 1700 6900
F 0 "#PWR027" H 1700 6650 50  0001 C CNN
F 1 "GND" H 1700 6750 39  0000 C CNN
F 2 "" H 1700 6900 50  0001 C CNN
F 3 "" H 1700 6900 50  0001 C CNN
	1    1700 6900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5B6E9CF9
P 1900 6900
F 0 "#PWR028" H 1900 6650 50  0001 C CNN
F 1 "GND" H 1900 6750 39  0000 C CNN
F 2 "" H 1900 6900 50  0001 C CNN
F 3 "" H 1900 6900 50  0001 C CNN
	1    1900 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6700 1300 6900
Wire Wire Line
	1500 6700 1500 6900
Wire Wire Line
	1700 6700 1700 6900
Wire Wire Line
	1900 6700 1900 6900
Text Notes 1200 6400 0    60   ~ 0
Mounting holes
Wire Wire Line
	3010 1820 3270 1820
Wire Wire Line
	1670 1320 1780 1320
Wire Wire Line
	1500 1720 1500 2360
Wire Wire Line
	1500 1320 1670 1320
Wire Wire Line
	3580 1820 3580 2360
Wire Wire Line
	3270 1320 3580 1320
Wire Wire Line
	3270 1820 3580 1820
Wire Wire Line
	3580 1320 3580 1400
Wire Wire Line
	1910 3340 2100 3340
Wire Wire Line
	2430 4780 2510 4780
Wire Wire Line
	2880 4780 3180 4780
Wire Wire Line
	3180 4780 3310 4780
Wire Wire Line
	4670 3340 4800 3340
Wire Wire Line
	4670 5040 4750 5040
Wire Wire Line
	2025 4780 2140 4780
Wire Wire Line
	1600 4505 1600 4580
Wire Wire Line
	6430 1210 6430 1290
Wire Wire Line
	6445 4635 6445 5275
Wire Wire Line
	7765 4040 7765 4235
Wire Wire Line
	8660 5140 8780 5140
Wire Wire Line
	4820 1425 4820 1625
Wire Wire Line
	4820 1625 4820 1780
Wire Wire Line
	8470 4635 8780 4635
Connection ~ 8780 4635
Wire Wire Line
	8780 4635 8780 4535
$Comp
L apx803:APX803-SA U3
U 1 1 5C9676FA
P 6430 1740
F 0 "U3" H 6152 1828 39  0000 R CNN
F 1 "APX803-SA" H 6152 1753 39  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6430 1740 50  0001 C CNN
F 3 "" H 6430 1740 50  0001 C CNN
	1    6430 1740
	1    0    0    -1  
$EndComp
Wire Wire Line
	7210 1210 7210 1365
Wire Wire Line
	7210 1565 7210 1740
Wire Wire Line
	7210 1740 6780 1740
Connection ~ 8520 4435
Wire Wire Line
	8520 4435 9170 4435
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5CC4FA38
P 950 3440
F 0 "J1" H 844 3215 39  0000 C CNN
F 1 "Conn_01x02_Female" H 844 3206 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 950 3440 50  0001 C CNN
F 3 "~" H 950 3440 50  0001 C CNN
	1    950  3440
	-1   0    0    1   
$EndComp
Wire Wire Line
	1230 3440 1150 3440
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 5CC5E282
P 9370 4435
F 0 "J2" H 9398 4365 39  0000 L CNN
F 1 "Conn_01x02_Female" H 9264 4201 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9370 4435 50  0001 C CNN
F 3 "~" H 9370 4435 50  0001 C CNN
	1    9370 4435
	1    0    0    -1  
$EndComp
Wire Wire Line
	8780 4535 9170 4535
Wire Wire Line
	5230 3340 6445 3340
Wire Wire Line
	5230 3800 5230 5230
Wire Wire Line
	1600 4980 1600 5230
Wire Wire Line
	1600 4120 1600 4230
Wire Wire Line
	3930 4130 3930 4580
$EndSCHEMATC
